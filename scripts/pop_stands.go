package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strconv"

	"gitlab.com/coopon/uauto/backend/uauto-backend-go.git/models"
	"gitlab.com/demonshreder/gosm"
)

func main() {
	osmFilePath, err := filepath.Abs("stands.xml")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	a, err := gosm.ParseXMLStruct(osmFilePath)

	for b := range a {
		id, _ := strconv.ParseUint(a[b].ID, 10, 64)
		stand := models.Stand{Lat: a[b].Lat, Lon: a[b].Lon, NodeID: id}
		// fmt.Println(strings.Replace(a[b].Lat, ".", "", -1))
		models.ORM.Create(&stand)
		fmt.Println(stand)
	}

	fmt.Println(a[0].ID)
}
