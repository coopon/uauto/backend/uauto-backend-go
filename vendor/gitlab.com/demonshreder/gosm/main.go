package gosm

// gosm is a XML parser for OSM XML files written in Go
// Copyright (C) 2018 Kamalavelan
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import (
	"encoding/xml"
	"fmt"
	"os"
)

// Nodes is the top level <osm>
type Nodes struct {
	XMLName xml.Name `xml:"osm"`
	Nodes   []Node   `xml:"node"`
}

// Node is the individual node
type Node struct {
	XMLName xml.Name `xml:"node"`
	ID      string   `xml:"id,attr"`
	Lat     string   `xml:"lat,attr"`
	Lon     string   `xml:"lon,attr"`
	Tags    []Tag    `xml:"tag"`
}

// Tag is the individual list of tags in a node
type Tag struct {
	XMLName xml.Name `xml:"tag"`
	Key     string   `xml:"k,attr"`
	Value   string   `xml:"v,attr"`
}

// ParseXMLStruct parses the OSM XML file and returns a struct
func ParseXMLStruct(osmFilePath string) ([]Node, error) {

	file, err := os.Open(osmFilePath)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	defer file.Close()
	// fmt.Println(ParseXMLStruct(file))

	var osmXML Nodes

	err = xml.NewDecoder(file).Decode(&osmXML)
	if err != nil {
		return nil, err
	}
	return osmXML.Nodes, err
}

// // ParseXMLMap parses the OSM XML file and returns a map
// func ParseXMLMap(file io.Reader) () {
//
// 	struct, _ := ParseXMLStruct(file)
//
// 	return ParseStructMap(struct)
//
// }
//
// // ParseStructMap parse the struct and returns a map
// func ParseStructMap(nodes []Node) () {
//
// }
//
// func main() {
// 	osmFilePath, err := filepath.Abs("data.xml")
// 	if err != nil {
// 		fmt.Println(err)
// 		os.Exit(1)
// 	}
//
// 	file, err := os.Open(osmFilePath)
// 	if err != nil {
// 		fmt.Println(err)
// 		os.Exit(1)
// 	}
//
// 	defer file.Close()
// 	fmt.Println(ParseXMLStruct(file))
//
// }
