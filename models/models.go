package models

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

//JSONResponse is doing something
type JSONResponse struct {
	Success bool   `json:"success"`
	Reason  string `json:"reason"`
}

type User struct {
	gorm.Model
	Mail     string `json:"mail"`
	Username string `json:"username"`
	Password string `json:"password"`
	Mobile   string `json:"mobile"`
	U_Type   int    `json:"u_type"`
	Name     string `json:"name"`
	License  string `json:"license"`
	StandID  uint   `json:"stand_id"`
	Vehicles []Vehicle

	// 0 passenger
	// 1 driver
	// 2 union member
	// 3 driver + union member
	// 4 root

}

type Vehicle struct {
	gorm.Model
	UserID       uint
	RegNumber    string
	Manufacturer string
	V_Model      string
	FullCheck    bool
	Ownership    User
}

type Stand struct {
	gorm.Model
	//	UserID uint
	Name   string
	Users  []User
	Lat    string
	Lon    string
	NodeID uint64
}

type Ride struct {
}

// ORM is the ORM
var ORM, err = gorm.Open("postgres", "user=pompom password=coopsarefun dbname=auto sslmode=disable")

// func main() {
// 	fmt.Println(err)
// 	ORM.Close()
// }
