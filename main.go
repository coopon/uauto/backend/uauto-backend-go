package main

import (
	"fmt"

	"gitlab.com/coopon/uauto/backend/uauto-backend-go.git/models"
	"gitlab.com/coopon/uauto/backend/uauto-backend-go.git/restapi"
	"gitlab.com/coopon/uauto/backend/uauto-backend-go.git/routes"
)

type Response struct {
	success bool
}

func main() {
	models.ORM.AutoMigrate(&models.User{}, &models.Stand{}, &models.Vehicle{})
	defer models.ORM.Close()
	models.ORM.Find(&restapi.Stands)
	// fmt.Println(&restapi.Stands)
	// restapi.Stands = "a"
	fmt.Println("uauto listening on http://127.0.0.1:8000")
	routes.Router()
}
