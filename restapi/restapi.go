package restapi

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"gitlab.com/coopon/uauto/backend/uauto-backend-go.git/models"
	"golang.org/x/crypto/bcrypt"
)

var store = sessions.NewCookieStore([]byte("something-very-secret"))

var standsUpdated = false

var Stands []models.Stand

// LoginValidateHandler - REST API login handler
func LoginValidateHandler(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r, "session-name")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	// b := "{'success':true}"
	b := models.JSONResponse{Success: true}
	json.NewEncoder(w).Encode(&b)

	fmt.Println(session.Values)
}

// LoginHandler - REST API login handler
func LoginHandler(w http.ResponseWriter, r *http.Request) {
	// vars := mux.Vars(r)
	// w.WriteHeader(http.StatusOK)
	if r.Method == "POST" {

		w.Header().Add("Content-Type", "application/json")
		var user models.User
		json.NewDecoder(r.Body).Decode(&user)
		password := user.Password
		// Simplistic check to see if the given input for user.Mail is mobile or email
		if !strings.Contains(user.Mail, ".com") && !strings.Contains(user.Mail, "@") {
			models.ORM.Where("Mobile = ?", user.Mail).Or("Mobile = ?", user.Mobile).First(&user)

		} else {
			models.ORM.Where("Mail = ?", user.Mail).First(&user)
		}

		err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))

		if err != nil {
			fmt.Println(err)
			b := models.JSONResponse{Success: false, Reason: "Username and Password do not match."}
			json.NewEncoder(w).Encode(&b)
		} else {
			fmt.Println("login success")
			session, err := store.Get(r, "session-name")
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			// Set some session values.
			session.Values["foo"] = "bar"
			session.Values[42] = 43
			// Save it before we write to the response/return from the handler.
			session.Save(r, w)
			b := models.JSONResponse{Success: true}
			// b := []byte(`{'success':true}`)
			json.NewEncoder(w).Encode(&b)
		}
	}
	// fmt.Fprintf(w, "Category: %v\n", vars["id"])
}

// UserHandler handles the user
func UserHandler(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {
		w.Header().Add("Content-Type", "application/json")
		vars := mux.Vars(r)
		if vars["id"] == "" {
			var u []models.User
			models.ORM.Find(&u)
			a, _ := json.Marshal(u)
			b := append([]byte(`{"success":true,"users":`), append(a, []byte(`}`)...)...)
			// w.WriteHeader(http.StatusCreated)
			w.Write(b)
			// fmt.Println(string(b))
		} else {
			id, _ := strconv.Atoi(vars["id"])
			// ID := uint(id)

			var u models.User
			// var j JSONResponse
			models.ORM.First(&u, "ID = ?", id)
			if u.ID == 0 {
				w.WriteHeader(http.StatusNotFound) //Code 204
			} else {
				a, _ := json.Marshal(u)
				b := append([]byte(`{"success":true,"users":`), append(a, []byte(`}`)...)...)
				// w.WriteHeader(http.StatusCreated)
				w.Write(b)
				// w.WriteHeader(http.StatusOK)
			}
		}
	} else if r.Method == "POST" {
		var user models.User
		json.NewDecoder(r.Body).Decode(&user)
		// fmt.Println(user.Username, user.Mail, user.ID, user.Mobile, user.Type, user.Password)
		hash, _ := bcrypt.GenerateFromPassword([]byte(user.Password), 7)
		user.Password = string(hash)
		models.ORM.Create(&user)
		w.WriteHeader(http.StatusCreated) //Code 201
	} else if r.Method == "DELETE" {
		vars := mux.Vars(r)
		// fmt.Println(vars)
		id, _ := strconv.Atoi(vars["id"])
		models.ORM.Where("ID = ?", id).Delete(models.User{})
		w.WriteHeader(http.StatusNoContent) //Code 204
	} else if r.Method == "PUT" {
		vars := mux.Vars(r)
		// fmt.Println(vars)
		id, _ := strconv.Atoi(vars["id"])
		models.ORM.Where("ID = ?", id).Update(models.User{})
		w.WriteHeader(http.StatusNoContent) //Code 204
	}
}

// StandHandler handles the stand
func StandHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		w.Header().Add("Content-Type", "application/json")
		vars := mux.Vars(r)
		if vars["id"] == "" {
			var s []models.Stand
			models.ORM.Find(&s)
			json.NewEncoder(w).Encode(&s)
			w.WriteHeader(http.StatusOK)

		} else {
			id, _ := strconv.Atoi(vars["id"])
			var s models.Stand
			models.ORM.First(&s, "ID = ?", id)
			if s.ID == 0 {
				w.WriteHeader(http.StatusNoContent)
			} else {
				json.NewEncoder(w).Encode(&s)
				w.WriteHeader(http.StatusOK)
			}
		}
	} else if r.Method == "POST" {
		var stand models.Stand
		json.NewDecoder(r.Body).Decode(&stand)
		models.ORM.Create(&stand)
		w.WriteHeader(http.StatusCreated) //Code 201
	} else if r.Method == "DELETE" {
		vars := mux.Vars(r)
		id, _ := strconv.Atoi(vars["id"])
		models.ORM.Where("ID = ?", id).Delete(models.Stand{})
		w.WriteHeader(http.StatusNoContent)
	} else if r.Method == "PUT" {
		vars := mux.Vars(r)
		// fmt.Println(vars)
		id, _ := strconv.Atoi(vars["id"])
		models.ORM.Where("ID = ?", id).Update(models.Stand{})
		w.WriteHeader(http.StatusNoContent) //Code 204
	}
}

// VehicleHandler handles the GET, POST, DELETE for /users/{id}/vehicles/
func VehicleHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		w.Header().Add("Content-Type", "application/json")
		vars := mux.Vars(r)
		if vars["id"] == "" {
			var v []models.Vehicle
			models.ORM.Find(&v)
			json.NewEncoder(w).Encode(&v)
			w.WriteHeader(http.StatusOK)

		} else {
			id, _ := strconv.Atoi(vars["id"])
			var v models.Vehicle
			models.ORM.First(&v, "ID = ?", id)
			if v.ID == 0 {
				w.WriteHeader(http.StatusNoContent)
			} else {

				json.NewEncoder(w).Encode(&v)
				w.WriteHeader(http.StatusOK)
			}
		}
	} else if r.Method == "POST" {
		var vehicle models.Vehicle
		json.NewDecoder(r.Body).Decode(&vehicle)
		models.ORM.Create(&vehicle)
		w.WriteHeader(http.StatusCreated)
	} else if r.Method == "DELETE" {
		vars := mux.Vars(r)
		id, _ := strconv.Atoi(vars["id"])
		models.ORM.Where("ID = ?", id).Delete(models.Vehicle{})
		w.WriteHeader(http.StatusNoContent)
	} else if r.Method == "PUT" {
		vars := mux.Vars(r)
		// fmt.Println(vars)
		id, _ := strconv.Atoi(vars["id"])
		models.ORM.Where("ID = ?", id).Update(models.Vehicle{})
		w.WriteHeader(http.StatusNoContent) //Code 204
	}
}

// RideSearchHandler handles the POST for /ride/search/
func RideSearchHandler(w http.ResponseWriter, r *http.Request) {
	if standsUpdated { // Future to retrieve new stand data in runtime
		fmt.Println("stands were updated")
	}

	if r.Method == "POST" {
		w.Header().Add("Content-Type", "application/json")
		var dat map[string]interface{}
		json.NewDecoder(r.Body).Decode(&dat)
		lat := dat["lat"].(string)
		lon := dat["lon"].(string)
		// fmt.Println(latLon)

		var data, distance map[string]interface{}
		var paths []interface{}
		// var DistanceInt, standNo. diint
		var url string
		dist, standNo, DistanceInt := 10000, 0, 0

		for x := range Stands {
			url = "http://localhost:8989/route?point=" + lat +
				"%2C" + lon + "&point=" + Stands[x].Lat + "%2C" + Stands[x].Lon

			resp, _ := http.Get(url)
			json.NewDecoder(resp.Body).Decode(&data)
			paths = data["paths"].([]interface{})
			distance = paths[0].(map[string]interface{})
			// distance["distance"]
			DistanceInt, _ = strconv.Atoi(strings.Split(fmt.Sprint(distance["distance"]), ".")[0])
			// fmt.Println(err)
			if DistanceInt <= dist {
				dist = DistanceInt
				standNo = x
			}
			// fmt.Println(x, distance["distance"], DistanceInt, dist)
		}
		// fmt.Println(standNo, Stands[standNo].Lat, Stands[standNo].Lon)
		w.Write([]byte(`{"lat":"` + Stands[standNo].Lat + `","lon":"` +
			Stands[standNo].Lon + `"}`))
	}
}

// RideRequestHandler handles the POST for /ride/search/
func RideRequestHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		w.Header().Add("Content-Type", "application/json")
	}
}

// RideAcceptHandler handles the POST for /ride/search/
func RideAcceptHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		w.Header().Add("Content-Type", "application/json")
	}
}
