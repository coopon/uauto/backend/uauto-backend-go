package routes

import (
	"net/http"
	"os"

	"gitlab.com/coopon/uauto/backend/uauto-backend-go.git/controllers"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"gitlab.com/coopon/uauto/backend/uauto-backend-go.git/restapi"
)

func Router() {
	r := mux.NewRouter()
	// r.StrictSlash(true) redirects to / instead of old url
	r.HandleFunc("/", controllers.IndexHandler)

	// Routes for all static pages
	r.HandleFunc("/home", controllers.HomeHandler).Methods("GET")
	r.HandleFunc("/home/", controllers.HomeHandler).Methods("GET")
	r.HandleFunc("/about", controllers.AboutHandler).Methods("GET")
	r.HandleFunc("/about/", controllers.AboutHandler).Methods("GET")
	r.HandleFunc("/blog", controllers.BlogHandler).Methods("GET")
	r.HandleFunc("/blog/", controllers.BlogHandler).Methods("GET")
	r.HandleFunc("/story", controllers.StoryHandler).Methods("GET")
	r.HandleFunc("/story/", controllers.StoryHandler).Methods("GET")
	//	r.HandleFunc("/api-docs", controllers.StoryHandler).Methods("GET")
	//	r.HandleFunc("/api-docs/", controllers.StoryHandler).Methods("GET")

	// Regular RESTAPI
	r.HandleFunc("/login", restapi.LoginHandler).Methods("POST")
	r.HandleFunc("/login/", restapi.LoginHandler).Methods("POST")
	r.HandleFunc("/login/validate", restapi.LoginValidateHandler).Methods("GET")
	r.HandleFunc("/login/validate/", restapi.LoginValidateHandler).Methods("GET")
	r.HandleFunc("/users", restapi.UserHandler).Methods("POST", "GET")
	r.HandleFunc("/users/", restapi.UserHandler).Methods("POST", "GET")
	//r.HandleFunc("/users/", handlers.LoggingHandler(os.Stdout, w))
	r.HandleFunc("/users/{id}", restapi.UserHandler).Methods("GET", "DELETE", "PUT")
	r.HandleFunc("/stands", restapi.StandHandler).Methods("POST", "GET")
	r.HandleFunc("/stands/", restapi.StandHandler).Methods("POST", "GET")
	r.HandleFunc("/stands/{id}", restapi.StandHandler).Methods("GET", "DELETE", "PUT")
	r.HandleFunc("/users/{id}/vehicles/", restapi.VehicleHandler).Methods("POST", "GET", "PUT")
	r.HandleFunc("/users/{id}/vehicles/{id}", restapi.VehicleHandler).Methods("GET", "DELETE", "PUT")
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {})

	//This allows to search for rides
	r.HandleFunc("/ride/search", restapi.RideSearchHandler).Methods("POST")
	r.HandleFunc("/ride/search/", restapi.RideSearchHandler).Methods("POST")
	r.HandleFunc("/ride/request", restapi.RideRequestHandler).Methods("POST")
	r.HandleFunc("/ride/request/", restapi.RideRequestHandler).Methods("POST")
	r.HandleFunc("/ride/accept", restapi.RideAcceptHandler).Methods("POST")
	r.HandleFunc("/ride/accept/", restapi.RideAcceptHandler).Methods("POST")

	// Code for fileserver, activate when not using proxy like NGINX
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/",
		http.FileServer(http.Dir("static/"))))

	loggedRouter := handlers.LoggingHandler(os.Stdout, r)
	http.ListenAndServe(":8000", loggedRouter)
}
