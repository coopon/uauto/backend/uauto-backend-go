package controllers

import (
	"html/template"
	"net/http"
	"os"
	"path/filepath"
)

var workDir, _ = os.Getwd()
var templatePath = filepath.Join(workDir, "templates/")

// IndexHandler is the landing page
func IndexHandler(w http.ResponseWriter, r *http.Request) {
	// fmt.Println(templatePath+"/static/base.html", templatePath+"/static/index.html")
	t := template.Must(template.ParseFiles(templatePath+"/static/base.html", templatePath+"/static/index.html"))
	// if err != nil {
	// 	fmt.Println(err)
	// }
	t.Execute(w, nil)
	// w.WriteHeader(http.StatusOK)
	// fmt.Fprintf(w, "Category: %v\n", vars["id"])
}

// HomeHandler is the landing page
func HomeHandler(w http.ResponseWriter, r *http.Request) {

	t := template.Must(template.ParseFiles(templatePath+"/app/base.html", templatePath+"/app/home.html"))
	t.Execute(w, nil)
	w.WriteHeader(http.StatusOK)
	// fmt.Fprintf(w, "Category: %v\n", vars["id"])
}

// BlogHandler is the landing page
func BlogHandler(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.ParseFiles(templatePath+"/static/base.html", templatePath+"/static/blog.html"))
	t.Execute(w, nil)
}

// AboutHandler is the landing page
func AboutHandler(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.ParseFiles(templatePath+"/static/base.html", templatePath+"/static/about.html"))
	t.Execute(w, nil)
}

// StoryHandler is the landing page
func StoryHandler(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.ParseFiles(templatePath+"/static/base.html", templatePath+"/static/story.html"))
	t.Execute(w, nil)
}
