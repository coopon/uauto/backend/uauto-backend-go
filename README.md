# Readme for uauto go backend

## Ultimate Go language reference

<https://gobyexample.com>

## Fetching the repo

`env GIT_TERMINAL_PROMPT=1 go get gitlab.com/coopon/uauto/backend/uauto-backend-go.git`

The code is now located at GOPATH (type "go env" in shell to see)
eg. ~/go/src/gitlab.com/coopon/uauto/backend/uauto-backend-go.git

## Requirements

Go 1.7+ (Latest Go for best compile times & performance)

Postgresql 9+


## Database settings

user - pompom

database - auto

password - coopsarefun

## Running

### bash shell

`go build -i -x -v main.go && ./main`

### fish shell

`go build -i -x -v main.go; and ./main`

### Serving static files (css, js, images)

- The program can serve static files you need to uncomment few lines
at the end of routes/routes.go

- You can use nginx with the following configuration

`location /static/ {
     root {GOPATH}/src/gitlab.com/coopon/uauto/backend/uauto-backend-go.git;
 }`

## Project structure

./
routes - all routes
restAPI - restAPI
controllers - controllers for the web frontend
static - static files for web frontend
static/app/ - contains the client side program through gopherjs
templates - templates for the web frontend
